import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./HomePage";
import { useState } from "react";
import Details from "./Details";
import NotFound from "./404";

export function App() {
  const [pokeDex, setPokeDex] = useState();
  const [allPokemons, setAllPokemons] = useState([]);
  const [loadMore, setLoadMore] = useState(
    "https://pokeapi.co/api/v2/pokemon?limit=20" //? Setting Limit Pokemon {20} default
  );

  const getAllPokemons = async () => {
    const res = await fetch(loadMore);
    const data = await res.json();

    setLoadMore(data.next);

    function createPokemonObject(results: any) {
      results.forEach(async (pokemon: any) => {
        const res = await fetch(
          `https://pokeapi.co/api/v2/pokemon/${pokemon.name}`
        );
        const data = await res.json();
        setAllPokemons((currentList): any => [...currentList, data]);
        await allPokemons.sort((a: any, b: any) => a.id - b.id);
      });
    }
    createPokemonObject(data.results);
  };
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <HomePage
              allPokemons={allPokemons}
              getAllPokemons={getAllPokemons}
              infoPokemon={(poke: any) => setPokeDex(poke)}
            />
          }
        />
        <Route path="/:name" element={<Details data={pokeDex} />} />

        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}
