/* eslint-disable react-hooks/rules-of-hooks */
import {
  Box,
  Heading,
  Text,
  Stack,
  Avatar,
  useColorModeValue,
  Image,
  SimpleGrid,
  Button,
  Center,
  HStack,
  Tag,
  Input,
  Container
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { useState } from "react";
import Hero from "./components/hero";
import Search from "./components/search";

const HomePage = ({ allPokemons, getAllPokemons, infoPokemon }: any) => {
  const [searchTerm, setSearchTerm] = useState("");

  return (
    <>
      <Hero />
      <Search setSearchTerm={setSearchTerm} />
      <Container maxW={"xxl"} p="12">
        <SimpleGrid
          columns={{ base: 1, md: 4, sm: 3, lg: 5 }}
          spacing={10}
          marginBottom={6}
        >
          {allPokemons
            .filter((val: any) => {
              if (searchTerm == "") {
                return val;
              } else if (
                val.name.toLowerCase().includes(searchTerm.toLowerCase())
              ) {
                return val;
              }
            })
            .map((results: any, index: any) => {
              return (
                <Link
                  key={index + 1}
                  to={`/${results.name}`}
                  onClick={() => infoPokemon(results)}
                >
                  <Box
                    maxW={"445px"}
                    w={"full"}
                    bg={useColorModeValue("white", "gray.900")}
                    boxShadow={"2xl"}
                    rounded={"md"}
                    p={6}
                    overflow={"hidden"}
                  >
                    <Box h={"210px"} mx={-6} pos={"relative"}>
                      <Center>
                        <Image
                          width={"70%"}
                          padding="10px"
                          height={"10rem"}
                          src={results.sprites.other.dream_world.front_default}
                        />
                      </Center>
                    </Box>
                    <Stack mt={"-38px"}>
                      <HStack spacing={2} mb={4}>
                        {results.types.map((results: any, index: any) => {
                          return (
                            <Tag
                              key={index + 1}
                              color={"green.500"}
                              textTransform={"uppercase"}
                              size={"md"}
                            >
                              {results.type.name}
                            </Tag>
                          );
                        })}
                      </HStack>
                      <Heading
                        color={useColorModeValue("gray.700", "white")}
                        fontSize={"2xl"}
                        fontFamily={"body"}
                      >
                        {results.name}
                      </Heading>
                      <Text color={"gray.500"}>
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                        sed
                      </Text>
                    </Stack>
                    <Stack
                      mt={6}
                      direction={"row"}
                      spacing={4}
                      align={"center"}
                    >
                      <Avatar
                        src={results.sprites.other.dream_world.front_default}
                      />
                      <Stack direction={"column"} spacing={0} fontSize={"sm"}>
                        <Text fontWeight={600}>#{results.order}</Text>
                        <Text color={"gray.500"}>Feb 08, 2021 · Samsudin</Text>
                      </Stack>
                    </Stack>
                  </Box>
                </Link>
              );
            })}
        </SimpleGrid>
        <Center>
          <Button
            size="md"
            height="48px"
            width="200px"
            border="2px"
            borderColor="green.500"
            onClick={() => getAllPokemons()}
          >
            Load More
          </Button>
        </Center>
      </Container>
    </>
  );
};

export default HomePage;
