import { Container, HStack, Input, useColorModeValue } from "@chakra-ui/react";
import React from "react";

function Search({ setSearchTerm }: any) {
  return (
    <>
      <Container maxW={"2xl"}>
        <HStack marginBottom={6} justifyContent="center">
          <Input
            width={"100%"}
            type={"text"}
            placeholder={"Name Pokemon"}
            color={useColorModeValue("gray.800", "gray.200")}
            bg={useColorModeValue("gray.100", "gray.600")}
            focusBorderColor="green.500"
            rounded={"full"}
            border={0}
            _focus={{
              bg: useColorModeValue("gray.200", "gray.800"),
              outline: "none"
            }}
            onChange={(event) => {
              setSearchTerm(event.target.value);
            }}
          />
        </HStack>
      </Container>
    </>
  );
}

export default Search;
